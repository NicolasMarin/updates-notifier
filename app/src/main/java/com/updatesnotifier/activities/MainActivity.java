package com.updatesnotifier.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.updatesnotifier.AppExecutors;
import com.updatesnotifier.Constants;
import com.updatesnotifier.MyAlertsManager;
import com.updatesnotifier.R;
import com.updatesnotifier.adapter.AlertRecyclerViewAdapter;
import com.updatesnotifier.database.AppDatabase;
import com.updatesnotifier.model.Alert;

import java.util.List;

public class MainActivity extends PermissionsActivity {
    private List<Alert> alerts;
    private AlertRecyclerViewAdapter adapter;
    private AppDatabase mDb;
    private BroadcastReceiver updateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDb = AppDatabase.getInstance(getApplicationContext());
        setUpUpdateReceiver();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(Constants.DEVELOPMENT_VERSION){//in this mode, the requests are made to locale files
            if(checkIfAlreadyHavePermission()){//check if it has permissions
                showPermissionGranted(true);
            }else{//if not, realize the request for the permission
                requestForSpecificPermission();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpRecyclerView();
        loadAlertsOnUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(updateReceiver);
    }

    public void openNewAlertActivity(View view){
        startActivity(new Intent(MainActivity.this, AlertDetailActivity.class));
    }

    /**
     * Set up a BroadcastReceiver that is called when a Notification that shows that the text to observ has changed.
     * When the BroadcastReceiver is called, update the content of the RecyclerView to change the state of the Alert modified from "A" to "D"
     */
    private void setUpUpdateReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.UPDATE_RECEIVER_ACTION);
        updateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpRecyclerView();
                loadAlertsOnUI();
            }
        };
        registerReceiver(updateReceiver, filter);
    }

    private void setUpRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new AlertRecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) { return false; }

            @Override// Called when a user swipes left or right on a ViewHolder
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int direction) {
                showAlert(viewHolder.getAdapterPosition());
            }
        }).attachToRecyclerView(recyclerView);
    }

    /**
     * Show an AlertDialog when the user try to delete an Alert, asking to confirm the deletion
     * if PositiveButton is selected, the Alert is errased.
     * if NegativeButton is selected, the Alert swiped out from the RecyclerView is reestablished.
     *
     * @param position the index of the alert that you want to confirm its deletion in the "alerts" list
     */
    private void showAlert(final int position) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_delete_message)
                .setTitle(R.string.dialog_delete_title)
                .setPositiveButton(R.string.dialog_delete_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AppExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() { deleteAlert(position); }
                        });
                    }
                })
                .setNegativeButton(R.string.dialog_delete_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.setAlertsOnRecyclerView(alerts);
                    }
                }).create().show();
    }

    /**
     * @param position the index of the alert to delete in the "alerts" list
     */
    private void deleteAlert(int position) {
        Alert alert = alerts.get(position);
        mDb.alertDao().delete(alert);
        if(alert.getState().equalsIgnoreCase("A"))
            new MyAlertsManager(getApplicationContext()).unScheduleAlert(alert);
        alerts.remove(position);
        adapter.notifyItemRemoved(position);
    }

    /**
     * Pass the alerts list to the adapter to update the RecyclerView
     */
    private void updateUi() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.setAlertsOnRecyclerView(alerts);
                Log.d(Constants.TAG, "Alert List size: " + alerts.size());
            }
        });
    }

    /**
     * Populate the "alerts" list from the database and refresh the Ui, out of the UiThread.
     */
    private void loadAlertsOnUI() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                alerts = mDb.alertDao().getAll();
                updateUi();
            }
        });
    }

    /**
     * It's used to close the app if Constants.DEVELOPMENT_VERSION is true,
     * and if the app has has not Permission to read external storage
     * @param granted if granted is false, close the application
     */
    public void showPermissionGranted(boolean granted){
        if(!granted) finish();
    }

}