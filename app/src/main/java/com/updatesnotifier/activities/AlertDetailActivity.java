package com.updatesnotifier.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.updatesnotifier.AppExecutors;
import com.updatesnotifier.Constants;
import com.updatesnotifier.MyAlertsManager;
import com.updatesnotifier.MyRequestManager;
import com.updatesnotifier.R;
import com.updatesnotifier.database.AppDatabase;
import com.updatesnotifier.model.Alert;

public class AlertDetailActivity extends AppCompatActivity {
    private long alertId;
    private AppDatabase mDb;
    private Intent intent;
    private EditText tvUrl, tvOriginalText, tvClassToSearch, tvSecondsBetweenIntervals;
    private Switch switchState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_detail);

        if(getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initVariables();

        //if the activity is used to update un alert
        if (intent != null && intent.hasExtra(Constants.UPDATE_ALERT_ID)){
            Button button = findViewById(R.id.btnSave);
            button.setText(R.string.save_button_update);

            alertId = intent.getLongExtra(Constants.UPDATE_ALERT_ID, -1);
            Log.d(Constants.TAG, "Update Alert with id: " + alertId);
            if(alertId != -1){
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        Alert alert = mDb.alertDao().findAlertById(alertId);
                        populateUI(alert);
                    }
                });
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){// Respond to the action bar's Up/Home button
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initVariables() {
        tvUrl = findViewById(R.id.url);
        tvOriginalText = findViewById(R.id.original_text);
        switchState = findViewById(R.id.switchState);
        tvClassToSearch = findViewById(R.id.class_to_search);
        tvSecondsBetweenIntervals = findViewById(R.id.seconds_between_intervals);
        mDb = AppDatabase.getInstance(getApplicationContext());
        intent = getIntent();
    }

    /**
     * Load all the view's contents of the Ui according to an Alert
     * @param alert It's used to set the values for the view's contents
     */
    private void populateUI(final Alert alert) {
        if (alert == null) return;
        tvUrl.setText(alert.getUrl());
        tvOriginalText.setText(alert.getOriginalText());
        tvClassToSearch.setText(alert.getClassToSearch());
        tvSecondsBetweenIntervals.setText(String.valueOf(alert.getSecondsBetweenIntervals()));
        runOnUiThread(new Runnable() {
            @Override
            public void run() { switchState.setChecked(alert.getState().equals("A")); }
        });
    }

    /**
     * Mehtod to respond to the Button onClick.
     * Validate that the values received as input are valid, then create an alert and is added/updated on the database
     * @param v The Button from the Ui
     */
    public void saveButtonClicked(View v) {
        if(!isAnAlertValid()) return;
        final Alert alert = getLoadedAlert();
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() { saveButtonRealizeAction(alert); }
        });
    }

    /**
     * @return Wheter the values received are valid to create an Alert object or not
     */
    private boolean isAnAlertValid() {
        if(isThereAnyEmptyField()){
            Toast.makeText(this, R.string.alert_detail_message_empty_fields, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!MyRequestManager.isAnUrlValid(tvUrl.getText().toString())){
            Toast.makeText(this, R.string.alert_detail_message_url_fail, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /**
     * @return An Alert created with the values received from the Ui's views
     */
    private Alert getLoadedAlert() {
        return new Alert(tvUrl.getText().toString(),
                Integer.parseInt(tvSecondsBetweenIntervals.getText().toString()),
                tvClassToSearch.getText().toString(), tvOriginalText.getText().toString(),
                switchState.isChecked() ? "A" : "D");
    }

    /**
     * According with if the Alert needs to be created or updated, it call the corresponding method
     * @param alert The alert to Create/Update
     */
    private void saveButtonRealizeAction(Alert alert) {
        if(intent.hasExtra(Constants.UPDATE_ALERT_ID))
            updateAlert(alert);
        else
            createAlertAndActivateNotification(alert);
    }

    /**
     * Check if is need to schedule, unschedule or reschedule an Alert,
     * then the Alert is updated in the database and finally the activity is closed.
     * @param currentAlert The Alert to Update
     */
    private void updateAlert(final Alert currentAlert) {
        Alert originalAlert = mDb.alertDao().findAlertById(alertId);
        currentAlert.setId(alertId);
        checkAlertsState(originalAlert, currentAlert);
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.alertDao().update(currentAlert);
                finish();
            }
        });
    }

    /**
     * According with the states from originalAlert and currentAlert schedule,
     * unschedule or reschedule the Alert
     */
    private void checkAlertsState(Alert originalAlert, Alert currentAlert) {
        MyAlertsManager alertsManager = new MyAlertsManager(getApplicationContext());
        if(compareAlertsStates(originalAlert, "D", currentAlert,"A")){
            alertsManager.scheduleAlert(currentAlert);
        }
        else if(compareAlertsStates(originalAlert, "A", currentAlert,"D")){
            alertsManager.unScheduleAlert(originalAlert);
        }
        else if(compareAlertsStates(originalAlert, "A", currentAlert,"A")){
            alertsManager.unScheduleAlert(originalAlert);
            alertsManager.scheduleAlert(currentAlert);
        }
    }

    /**
     * @return if the state from originalAlert is equal to originalState and
     *         if the state from currentAlert is equal to currentState
     */
    private boolean compareAlertsStates(Alert originalAlert, String originalState, Alert currentAlert, String currentState) {
        return originalAlert.getState().equalsIgnoreCase(originalState) &&
                currentAlert.getState().equalsIgnoreCase(currentState);
    }

    /**
     * @param alert This Alert object is persisted in the database,
     *              according to it's state the alert is registered and the activity is ended.
     */
    private void createAlertAndActivateNotification(Alert alert) {
        alertId = mDb.alertDao().insert(alert);
        if(alert.getState().equals("A")){
            alert.setId(alertId);
            new MyAlertsManager(this).scheduleAlert(alert);
        }
        finish();
    }

    /**
     * @return if any of the TextView from the Ui is empty
     */
    private boolean isThereAnyEmptyField() {
        return isEmpty(tvSecondsBetweenIntervals) || isEmpty(tvUrl)
                || isEmpty(tvOriginalText) || isEmpty(tvClassToSearch);
    }

    private boolean isEmpty(EditText tv) { return tv.getText().toString().isEmpty(); }
}