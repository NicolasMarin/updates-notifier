package com.updatesnotifier.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.updatesnotifier.Constants;
import com.updatesnotifier.service.RequestHandlerService;

public class AlertReceiver extends BroadcastReceiver{

    /**
     * This Receiver is called in every interval from the {@link android.app.AlarmManager}'s setRepeating method.
     * Then start the Service {@link RequestHandlerService} passing the Alert id to check as parameter
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(Constants.TAG, "AlertReceiver onReceive");
        Intent intentService = new Intent(context, RequestHandlerService.class);
        intentService.putExtra(Constants.ALERT_ID, intent.getLongExtra(Constants.ALERT_ID, -1));
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            context.startForegroundService(intentService);
        }else {
            context.startService(intentService);
        }
    }
}
