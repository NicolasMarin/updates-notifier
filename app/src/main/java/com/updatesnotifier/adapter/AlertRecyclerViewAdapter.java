package com.updatesnotifier.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.updatesnotifier.Constants;
import com.updatesnotifier.R;
import com.updatesnotifier.activities.AlertDetailActivity;
import com.updatesnotifier.model.Alert;

import java.util.List;

public class AlertRecyclerViewAdapter extends RecyclerView.Adapter<AlertRecyclerViewAdapter.AlertViewHolder> {
    private List<Alert> mAlerts;
    private Context context;

    public AlertRecyclerViewAdapter(Context context) { this.context = context; }

    @NonNull
    @Override
    public AlertViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View alertView = inflater.inflate(R.layout.alert_item, parent, false);
        return new AlertViewHolder(alertView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlertViewHolder holder, int i) {
        holder.tvUrl.setText(mAlerts.get(i).getUrl());
        holder.tvOriginalText.setText(mAlerts.get(i).getOriginalText());
        if(mAlerts.get(i).getState().equalsIgnoreCase("A"))
            holder.ivState.setBackground(ContextCompat.getDrawable(context, R.color.holo_green_light));
    }

    @Override
    public int getItemCount() { return mAlerts == null ? 0 : mAlerts.size(); }

    /**
     * The ViewHolder used to represent an specific Alert in the RecyclerView
     */
    class AlertViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvUrl, tvOriginalText;
        private ImageView ivState;

        public AlertViewHolder(View itemView){
            super(itemView);
            tvUrl = itemView.findViewById(R.id.url);
            tvOriginalText = itemView.findViewById(R.id.original_text);
            ivState = itemView.findViewById(R.id.state);
            itemView.setOnClickListener(this);
        }

        /**
         * This method respond to the onClick event from an AlertViewHolder (item from the RecyclerView)
         * and calls to {@link AlertDetailActivity} to update the Alert selected
         * @param v The AlertViewHolder that is clicked
         */
        @Override
        public void onClick(View v) {
            long alertId = mAlerts.get(getAdapterPosition()).getId();
            Intent i = new Intent(context, AlertDetailActivity.class);
            i.putExtra(Constants.UPDATE_ALERT_ID, alertId);
            context.startActivity(i);
        }
    }

    /**
     * @param alertList The content from this list of Alerts replace the mAlerts's content,
     *                  then notify that the data set has changed
     */
    public void setAlertsOnRecyclerView(List<Alert> alertList) {
        mAlerts = alertList;
        notifyDataSetChanged();
    }
}
