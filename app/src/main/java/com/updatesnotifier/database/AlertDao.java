package com.updatesnotifier.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.updatesnotifier.model.Alert;

import java.util.List;

@Dao
public interface AlertDao {

    @Query("SELECT * FROM alert ORDER BY id")
    List<Alert> getAll();

    @Query("SELECT * FROM alert WHERE id LIKE :idSerach LIMIT 1")
    Alert findAlertById(long idSerach);

    @Insert
    long insert(Alert alert);

    @Update
    void update(Alert alert);

    @Delete
    void delete(Alert user);
}





