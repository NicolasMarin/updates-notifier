package com.updatesnotifier.database;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.updatesnotifier.Constants;
import com.updatesnotifier.model.Alert;

@Database(entities = {Alert.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "alertlist";
    private static AppDatabase sInstance;

    public static AppDatabase getInstance(Context context) {
        String message = "";
        if (sInstance == null) {
            synchronized (LOCK) {
                message = "Creating new database instance, ";
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        AppDatabase.class, AppDatabase.DATABASE_NAME).build();
            }
        }
        Log.d(Constants.TAG, message + "Getting the database instance");
        return sInstance;
    }

    public abstract AlertDao alertDao();

}
