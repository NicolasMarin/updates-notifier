package com.updatesnotifier;

public class Constants {

    public static boolean DEVELOPMENT_VERSION = false;
    public static String TAG = "INFO";
    public static final String NOTIFICATION_CHANNEL_ID2 = "10000";
    public static final String NOTIFICATION_CHANNEL_ID1 = "10001";
    public static String NOTIFICATION_CHANNEL_NAME1 = "service-notification-channel";
    public static String NOTIFICATION_CHANNEL_NAME2 = "response-notification-channel";

    public static String ALERT_ID = "alert-registry-id";
    public static String UPDATE_ALERT_ID = "update-alert-id";
    public static String REQUEST_BAD_RESULT = "Bad";
    public static String UPDATE_RECEIVER_ACTION = "update-receiver-action";
    public static String UPDATE_RECEIVER_ALERT_ID = "update-receiver-alert-id";

}
