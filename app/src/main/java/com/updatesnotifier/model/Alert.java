package com.updatesnotifier.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "alert")
public class Alert {

    @PrimaryKey(autoGenerate = true)                private long id;
    @ColumnInfo(name = "url")                       private String url;
    @ColumnInfo(name = "seconds_between_intervals") private int secondsBetweenIntervals;
    @ColumnInfo(name = "class_to_search")           private String classToSearch;
    @ColumnInfo(name = "original_text")             private String originalText;
    @ColumnInfo(name = "state")                     private String state;

    public Alert(long id, String url, int secondsBetweenIntervals, String classToSearch, String originalText, String state) {
        this.id = id;
        this.url = url;
        this.secondsBetweenIntervals = secondsBetweenIntervals;
        this.classToSearch = classToSearch;
        this.originalText = originalText;
        this.state = state;
    }

    @Ignore
    public Alert(String url, int secondsBetweenIntervals, String classToSearch, String originalText, String state) {
        this.url = url;
        this.secondsBetweenIntervals = secondsBetweenIntervals;
        this.classToSearch = classToSearch;
        this.originalText = originalText;
        this.state = state;
    }

    public long getId() { return id; }

    public String getUrl() { return url; }

    public int getSecondsBetweenIntervals() { return secondsBetweenIntervals; }

    public String getClassToSearch() { return classToSearch; }

    public String getOriginalText() { return originalText; }

    public String getState() { return state; }

    public void setId(long id) { this.id = id; }

    public void setUrl(String url) { this.url = url; }

    public void setSecondsBetweenIntervals(int secondsBetweenIntervals) { this.secondsBetweenIntervals = secondsBetweenIntervals; }

    public void setClassToSearch(String classToSearch) { this.classToSearch = classToSearch; }

    public void setOriginalText(String originalText) { this.originalText = originalText; }

    public void setState(String state) { this.state = state; }

    @Override
    public String toString() {
        return "Alert{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", secondsBetweenIntervals=" + secondsBetweenIntervals +
                ", classToSearch='" + classToSearch + '\'' +
                ", originalText='" + originalText + '\'' +
                ", state='" + state + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Alert alert = (Alert) o;
        return id == alert.id &&
                secondsBetweenIntervals == alert.secondsBetweenIntervals &&
                url.equalsIgnoreCase(alert.url) &&
                classToSearch.equalsIgnoreCase(alert.classToSearch) &&
                originalText.equalsIgnoreCase(alert.originalText) &&
                state.equalsIgnoreCase(alert.state);
    }
}
