package com.updatesnotifier.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.updatesnotifier.AppExecutors;
import com.updatesnotifier.Constants;
import com.updatesnotifier.MyAlertsManager;
import com.updatesnotifier.MyRequestManager;
import com.updatesnotifier.R;
import com.updatesnotifier.activities.AlertDetailActivity;
import com.updatesnotifier.database.AppDatabase;
import com.updatesnotifier.model.Alert;

import java.util.ArrayList;
import java.util.List;

public class RequestHandlerService extends Service {
    public static List<Long> ids;
    private static int SERVICE_NOTIFICATION_ID = 1;

    @Override
    public void onCreate() {
        startForeground(SERVICE_NOTIFICATION_ID, getServiceNotification());
        Log.d(Constants.TAG, "Service onCreate");
        ids = new ArrayList<>();
        super.onCreate();
    }

    /**
     * @return A Notification showed in Foreground while the Service is working
     */
    private Notification getServiceNotification() {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification notification = new NotificationCompat.Builder(this, Constants.NOTIFICATION_CHANNEL_ID1)
                .setContentTitle(getResources().getString(R.string.notification_backgroud_service_message))
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setAutoCancel(true)
                .setChannelId(Constants.NOTIFICATION_CHANNEL_ID1).build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel(Constants.NOTIFICATION_CHANNEL_ID1,
                    Constants.NOTIFICATION_CHANNEL_NAME1, NotificationManager.IMPORTANCE_LOW);
            if(manager != null) manager.createNotificationChannel(channel);
        }
        if(manager != null) manager.notify(SERVICE_NOTIFICATION_ID, notification);
        return notification;
    }

    /**
     * Use a list of Alert's Ids, and check to avoid to handle more than one Request from the same Alert at the same time.
     * For the Alert corresponding to the alertId, use the RequestHandler to verify if the text to observ has changed.
     * In that case notify the user with a Notification.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final long alertId = intent.getLongExtra(Constants.ALERT_ID, -1);
        if(!ids.contains(alertId)){
            Log.d(Constants.TAG, "Service onStartCommand, alertId received: " + alertId);
            ids.add(alertId);
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    Alert alert = AppDatabase.getInstance(getApplicationContext()).alertDao().findAlertById(alertId);
                    RequestHandler content = new RequestHandler();
                    content.setVariables(alert, getApplicationContext(), alertId);
                    content.execute(null, null, null);
                }
            });
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(Constants.TAG, "Service onDestroy");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private static class RequestHandler extends AsyncTask<Void, Void, Void> {
        private String requestResult = "";
        private Alert alert;
        private Context context;
        private int REQUEST_CODE = 0;
        private long currentId = -1;

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            showResults();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if(alert != null && context != null && currentId != -1) {
                requestResult = MyRequestManager.getRequestResult(alert);
            }
            return null;
        }

        public void setVariables(Alert alert, Context context, long alertId){
            this.alert = alert;
            this.context = context;
            this.currentId = alertId;
        }

        /**
         * Check if the OriginalText from the alert has changed.
         * In that case, shows a Notification to the user, unschedule the alert and
         * update the {@link com.updatesnotifier.activities.MainActivity}'s UI.
         * In any case, remove the currentId from the ids list and if that list is empty, stop the service.
         */
        public void showResults(){
            if(alert == null || context == null || currentId == -1) return;
            boolean didTextChange = !alert.getOriginalText().equals(requestResult);

            if(didTextChange){
                showChangeNotification();
                new MyAlertsManager(context).unScheduleAlert(alert);
                updateAppUI();
                Log.d(Constants.TAG, "The value of the alert with id: " + alert.getId() + " change and now is: " + alert.getOriginalText());
            }else{
                Log.d(Constants.TAG, "The value of the alert with id: " + alert.getId() + " still is: " + alert.getOriginalText());
            }

            ids.remove(currentId);
            Log.d(Constants.TAG, "ids List size: " + ids.size());
            if (ids.size() == 0) context.stopService(new Intent(context.getApplicationContext(), RequestHandlerService.class));
        }

        /**
         * Send a Broadcast to Update the UI from {@link com.updatesnotifier.activities.MainActivity}
         */
        private void updateAppUI() {
            Intent local = new Intent();
            local.setAction(Constants.UPDATE_RECEIVER_ACTION);
            local.putExtra(Constants.UPDATE_RECEIVER_ALERT_ID, alert.getId());
            context.sendBroadcast(local);
        }

        /**
         * Shows the Notification that indicates an Alert's OriginalText has changed
         */
        private void showChangeNotification() {
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = getChangeNotification(alert);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                NotificationChannel channel = new NotificationChannel(Constants.NOTIFICATION_CHANNEL_ID2,
                        Constants.NOTIFICATION_CHANNEL_NAME2, NotificationManager.IMPORTANCE_HIGH);
                if(manager != null) manager.createNotificationChannel(channel);
            }
            int id = (int) alert.getId() + 1;
            if(manager != null) manager.notify(id, notification);
        }

        /**
         * @param alert Alert of which the OriginalText has changed.
         * @return A Notification that inform that the OriginalText from alert has changed and
         * when the user presses it, it takes you to {@link AlertDetailActivity}.
         */
        private Notification getChangeNotification(Alert alert) {
            Intent intent = new Intent(context, AlertDetailActivity.class);
            long elementId = alert.getId();
            intent.putExtra(Constants.UPDATE_ALERT_ID, elementId);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntent(intent);
            PendingIntent pendingIntent = stackBuilder.getPendingIntent(REQUEST_CODE, PendingIntent.FLAG_UPDATE_CURRENT);

            String contentText = context.getResources().getString(R.string.notification_content_text_start) + alert.getOriginalText() + context.getResources().getString(R.string.notification_content_text_end);
            return new NotificationCompat.Builder(context, Constants.NOTIFICATION_CHANNEL_ID2)
                    .setContentTitle(context.getResources().getString(R.string.notification_content_title))
                    .setContentText(contentText)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setChannelId(Constants.NOTIFICATION_CHANNEL_ID2).build();
        }
    }
}
