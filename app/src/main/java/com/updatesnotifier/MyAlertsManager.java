package com.updatesnotifier;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.updatesnotifier.database.AppDatabase;
import com.updatesnotifier.model.Alert;
import com.updatesnotifier.receiver.AlertReceiver;

public class MyAlertsManager {
    private AlarmManager alarmManager;
    private Context context;

    public MyAlertsManager(Context context) { this.context = context; }

    /**
     * It schedule to repeat every so often the call to {@link AlertReceiver} to check the Text to Observ.
     * @param alert Alert from which it get the time between intervals
     */
    public void scheduleAlert(Alert alert) {
        long secondsBetweenIntervals = alert.getSecondsBetweenIntervals();
        PendingIntent pendingIntent = getPendingIntentAlarmManager(alert);
        if (alarmManager != null) {
            alarmManager.cancel (pendingIntent);
            Log.d(Constants.TAG, "Activate Alert with id: " + alert.getId() + ", every " + alert.getSecondsBetweenIntervals() + " seconds.");
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    5000, secondsBetweenIntervals * 1000, pendingIntent);
        }
    }

    /**
     * It unschedule the alert and update it's state in the database to "D".
     * @param alert Alert from which it get the id to unschedule.
     */
    public void unScheduleAlert(final Alert alert) {
        PendingIntent pendingIntent = getPendingIntentAlarmManager(alert);
        if (alarmManager != null){
            alarmManager.cancel (pendingIntent);
            Log.d(Constants.TAG, "Desactivate alert with id: " + alert.getId());
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    alert.setState("D");
                    AppDatabase.getInstance(context).alertDao().update(alert);
                }
            });
        }
    }

    /**
     * @param alert that the {@link AlertReceiver} will check if it's originalText has changed.
     * @return The PendingIntent used to schedule/unschedule the repeated
     * call to {@link AlertReceiver} to check the originalText.
     */
    private PendingIntent getPendingIntentAlarmManager(Alert alert) {
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context.getApplicationContext(), AlertReceiver.class);
        intent.putExtra(Constants.ALERT_ID, alert.getId());
        int requestCode = (int) alert.getId();
        return PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
