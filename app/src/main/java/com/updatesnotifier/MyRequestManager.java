package com.updatesnotifier;

import android.util.Log;
import android.webkit.URLUtil;

import com.updatesnotifier.model.Alert;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

public class MyRequestManager {

    /**
     * @return wheter the url can be used to make a request in the moment.
     */
    public static boolean canDoTheRequest(String url) {
        if(Constants.DEVELOPMENT_VERSION){
            File file = new File(url);
            return file.exists() && file.canRead();
        }else {
            return getStatusConnectionCode(url) == 200;
        }
    }

    /**
     * @param alert The Alert from which get the Url and ClassToSearch
     * @return The current text value from the ClassToSearch on the web page with the Url from the Alert.
     *         If it can't get this value, return Constants.REQUEST_BAD_RESULT
     */
    public static String getRequestResult(Alert alert) {
        if(canDoTheRequest(alert.getUrl())){
            Document document = getHtmlDocument(alert.getUrl());
            if(document != null) {
                Elements entryClassElements = document.getElementsByClass(alert.getClassToSearch());
                if(entryClassElements.size() > 0) {
                    Element entryClass = entryClassElements.get(0);
                    return entryClass.ownText();
                }
            }
        }
        return Constants.REQUEST_BAD_RESULT;
    }

    /**
     * With this method get an object of the Document class with the HTML content
     * of the web (or from a file) that will allow me to parse it with the methods of the JSoup library
     */
    public static Document getHtmlDocument(String url) {
        try{
            if(Constants.DEVELOPMENT_VERSION){
                return Jsoup.parse(new File(url), "UTF-8", "http://www.google.com");
            }else{
                return Jsoup.connect(url).userAgent("Mozilla/5.0")
                        .referrer("http://www.google.com").timeout(100000).get();
            }
        } catch (IOException ex) {
            Log.d(Constants.TAG,"Exception when obtaining the HTML of the page" + ex.getMessage());
            return null;
        }
    }

    /**
     * With this method I check the Status code of the response I receive when making the request
     * 		200 OK					300 Multiple Choices
     * 		301 Moved Permanently	305 Use Proxy
     * 		400 Bad Request			403 Forbidden
     * 		404 Not Found			500 Internal Server Error
     * 		502 Bad Gateway			503 Service Unavailable
     */
    public static int getStatusConnectionCode(String url) {
        try {
            return Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(1000000)
                    .ignoreHttpErrors(true).execute().statusCode();
        } catch (IOException ex) {
            Log.d(Constants.TAG,"Exception when obtaining the Status Code: " + ex.getMessage());
            return -1;
        }
    }

    /**
     * @return wheter the url string received is actually a valid URL.
     */
    public static boolean isAnUrlValid(String url) {
        if(Constants.DEVELOPMENT_VERSION){
            File file = new File(url);
            return file.exists() && file.canRead();
        }else {
            return URLUtil.isValidUrl(url);
        }
    }
}