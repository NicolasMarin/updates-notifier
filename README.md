# Updates Notifier [![build status](https://gitlab.com/NicolasMarin/updates-notifier/badges/master/pipeline.svg?job=assembleDebug&key_text=Build)](https://gitlab.com/NicolasMarin/updates-notifier/-/commits/master)

Una Aplicación Android para registrar alertas que comprueban si algún contenido de una Página Web ha cambiado de acuerdo con el atributo class y su texto de las etiquetas html.


## Prerequisitos

Para instalar la Aplicación tenemos 2 formas.

Primero, si solo queremos correr la Aplicación, podemos instalarla en nuestro teléfono Android usando el archivo .apk.
Para hacer esto, copiamos el archivo "app-debug.apk" del directorio "\app\build\salidas\apk\debug" en este proyecto a nuestro teléfono Android.
Desde el teléfono, abrimos el archivo "app-debug.apk", respondemos que queremos instalar la aplicación, y eso es todo. La aplicación está lista para que la usemos.

La otra forma, en caso de que, además de ejecutar la aplicación, también queramos ver y/o modificar el código, entonces podemos ejecutar la Aplicación usando el IDE Android Studio.

Desde Android Studio, vamos a "File" -> "New" -> "Project from Version Control" y copiamos la URL del repositorio de este proyecto.
Una vez que el proyecto está abierto en el IDE y después de que finaliza el buildeo, conectamos el teléfono a la computadora a través de un cable USB.
Para poder instalar y ejecutar la Aplicación en nuestro teléfono desde Android Studio, tendremos que acceder a las "Opciones de Desarrollador" y habilitar la "Depuración por USB".
Ahora, en Android Studio, con nuestro teléfono seleccionado como el dispositivo en el que se ejecutan las aplicaciones, hacemos clic en el botón "Run" y un poco más tarde la aplicación se instalará y se iniciará en el teléfono.


## Instrucciones para usar

Al iniciar la Aplicación, veremos una ventana con la lista de Alertas creadas.

<p float="left">
  <img src="images/Screenshot_01.png" width="350" alt="Imagen de la Ventana Principal"/>

  <img src="images/Screenshot_02.png" width="530" alt="Imagen de la Ventana Principal"/>
</p>

Cada uno de estos items contiene:

 - la URL de la Página Web para revisar

 - el Texto Original que la Aplicación verificará si ha cambiado

 - el Estado de la Alerta (una Alerta puede estar Activada o Desactivada)

<p float="left">
  <img src="images/Screenshot_03_Item_arrows.png" width="350" alt="Imagen de una Alerta de la lista"/>

  <img src="images/Screenshot_04_Item_arrows.png" width="350" alt="Imagen de una Alerta de la lista"/>
</p>

Desde aquí, podemos crear, actualizar o eliminar cualquier Alerta.

### Crear una Alerta

Para crear una alerta, hacemos clic en el botón con el "+" (más), que nos llevará a la Ventana Crear.

<img src="images/Screenshot_05.png" alt="Imagen de la ventana para crear una Alerta"/>


Aquí, tendremos que ingresar los valores para la nueva Alerta. Además de la URL, el Texto Original y el Estado de la Alerta, tendremos:

- el Valor de Class, es el valor del atributo Class en las etiquetas HTML de la Página en la que la Aplicación verificará si su texto ha cambiado de ser el que está en "Texto Original".

- los segundos, es el tiempo aproximado entre cada repetición de la revisión de la Página Web.

Con todos los campos llenos, presionaremos el botón "GUARDAR" para crear la Alerta y volver a la Ventana Principal.

### Actualizar una Alerta

Para actualizar una Alerta desde la Ventana Principal, hacemos clic en la Alerta que queremos actualizar y la Aplicación nos llevará a la Ventana de Actualización.

<img src="images/Screenshot_06.png" alt="Imagen de la ventana para actualizar una Alerta"/>


En esta Ventana, simplemente cambiaremos algún valor de la Alerta seleccionada y presionaremos el botón "Actualizar" para guardar sus cambios y volver a la Ventana Principal.

### Borrar una Alerta

Para eliminar una Alerta de la Ventana Principal, solo tendremos que deslizarla hacia un lado. Entonces aparecerá un Dialogo para que confirmemos la eliminación.

<img src="images/Screenshot_07.png" width="400" alt="Imagen del dialogo para confirmar el borrado de una Alerta"/>


### Estado de una Alerta

Si el Estado de una Alerta es "Desactivado", solo se registrará, pero la Aplicación no comenzará a verificar la Página web.

Si su Estado es "Activado", cada vez que pase la cantidad de tiempo especificada, la Aplicación verificará si la etiqueta HTML con el class especificado cambió el valor de su texto del que fue especificado. Si el texto ha cambiado, el Estado de la Alerta pasará a "Desactivado" y aparecerá una Notificación para informarnos que el valor de dicha Alerta ha cambiado.

<img src="images/Screenshot_08.png" width="400" alt="Imagen de la Notificacion informando que el texto cambio"/>


## Construido Con

* [Room](https://developer.android.com/jetpack/androidx/releases/room) - Libreria utilizada para persistir las Alertas
* [Jsoup](https://jsoup.org/) - Libreria utilizada para realizar el Web Scrapping de la Pagina Web de la que se busca que la aplicacion nos notifique si hay un cambio
* [AlarmManager](https://developer.android.com/reference/android/app/AlarmManager) - Clase utilizada para registrar la repeticion del chequeo de la Pagina Web
* [Service](https://developer.android.com/reference/android/app/Service), [BroadcastReceiver](https://developer.android.com/reference/android/content/BroadcastReceiver) - Clases llamadas por AlarmManager para que estas revisen la Pagina Web en busca de cambios
* [RecyclerView](https://developer.android.com/reference/androidx/recyclerview/widget/RecyclerView) - Clase utilizada como View en la cual listar las Alertas creadas
* [Notification](https://developer.android.com/reference/android/app/Notification) - Clase utilizada para crear las Notificaciones que informan al usuario cuando la Aplicacion detecta que la Pagina Web de una Alerta ha cambiado
